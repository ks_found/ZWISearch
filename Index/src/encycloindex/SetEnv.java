package encycloindex;

import java.io.*;
import java.util.*;
import org.apache.commons.lang.StringUtils;

import org.jsoup.safety.Whitelist;
import org.jsoup.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;

public final class SetEnv {


	private static final Pattern REMOVE_TAGS = Pattern.compile("<.+?>");


	static public String[] common = {"class", "public", "private", "protected", "method", "the","of","and","a","to","in","is","you","that","it","he","was","for","on","are","as","with","his","they","I","at","be","this","have","from","or","one","had","by","word","but","not","what","all","were","we","when","your","can","said","there","use","an","each","which","she","do","how","their","if","will","up","other","about","out","many","then","them","these","so","some","her","would","make","like","him","into","time","has","look","two","more","write","go","see","no","way","could","my","than","been","who","oil","its","now","down","did","get"};


	static public String removeScriptContent(String filename) {


		String tmp="";


		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			tmp  = sb.toString();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();

		}


		return tmp;

	}





	/**
	 * Get text between two strings. Passed limiting strings are not 
	 * included into result.
	 *
	 * @param text     Text to search in.
	 * @param textFrom Text to start cutting from (exclusive).
	 * @param textTo   Text to stop cuutting at (exclusive).
	 */
	public static String getBetweenStrings(
	    String text,
	    String textFrom,
	    String textTo) {

		String tmp=text;
		int ind1=text.indexOf(textFrom);
		int ind2=text.lastIndexOf(textTo);

		int len=textFrom.length();

		//System.out.println("Start="+textFrom+ " End="+textTo);
		//System.out.println(ind2);
		//System.out.println(ind1+len);

		if (ind2>ind1+len)
			tmp=text.substring(ind1+len,ind2);

		return tmp;

		//  return StringUtils.substringBetween(text, textFrom, textTo);

	}



	// get TXT string with article. Optionally, skip non-body parits
	public static String getCleanTXT(String html) {

		String start="<!-- BEGIN BODY -->";
		String end="<!-- END BODY -->";
		String txthtml=SetEnv.getBetweenStrings(html,start,end);
		String txt=Jsoup.parse(txthtml).text();
		txt=txt.trim();

		return txt;


	}


	// get String from zip
	static public String  getStringFromZip(String key, ZipFile zipFile) throws IOException {


		String tmp="";
		ZipEntry ze = zipFile.getEntry(key);
		long size = ze.getSize();
		if (size > 0) {
			BufferedReader br;
			try {
				br = new BufferedReader(new InputStreamReader(zipFile.getInputStream(ze)));
				String line;
				while ((line = br.readLine()) != null) {
					tmp=tmp+line;
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}


		return tmp;
	}

    /**
     *
     * Get a short 12-char hash of the string. 
     * */

    public static String getSHA(String input)
    {
        try {
            // getInstance() method is called with algorithm SHA-1
            MessageDigest md = MessageDigest.getInstance("SHA-256");
	    byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            String hashtext = Base64.getEncoder().encodeToString(hash);
            // return the HashText up to 12 
            return hashtext.substring(0,12);
        }
 
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }










public static String removeTags(String string) {
    if (string == null || string.length() == 0) {
        return string;
    }

    Matcher m = REMOVE_TAGS.matcher(string);
    return m.replaceAll("");
}


}
