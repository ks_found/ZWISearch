package encycloreader;

import java.io.*;
import java.util.*;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
// import org.apache.lucene.analysis.standard.ClassicAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;

import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.store.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.*;
import org.apache.lucene.analysis.Analyzer;
import encycloindex.*;
import java.time.Instant;

import java.nio.file.Path;
import java.text.SimpleDateFormat;
import org.apache.lucene.codecs.Codec;
//import org.apache.lucene.codecs.lucene87.Lucene87Codec;
//import org.apache.lucene.codecs.lucene87.Lucene87StoredFieldsFormat;
import org.apache.lucene.util.BytesRef;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.*;
import java.io.*;
import java.util.Enumeration;
import java.util.zip.*;
import java.util.zip.ZipFile;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.tmatesoft.sqljet.core.*;
import org.tmatesoft.sqljet.core.schema.*;
import org.tmatesoft.sqljet.core.table.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneOffset;
import java.text.DateFormat;
import java.util.TimeZone;


// Dump structure: https://meta.wikimedia.org/wiki/Data_dumps/Dump_format
public class IndexZWI
{


	private static Long  timeStampSeconds=0L;
	private static final Long MaxTime = 28800L; // 8h is max time for update
	private static final String confile="search.properties";
	private static String INDEX_DIR = "./lucene_index";
	private static String ls = System.getProperty("line.separator");
	private static final String ZWI_INDEX = "/var/www/html/encycloreader/assets/dbase/ZWI/en/index.sqlite";
	private static final String ZWI_PATH = "/var/www/html/encycloreader/assets/dbase/ZWI/en/";
	private static  Map<String, String> existingTitleSha1 = new HashMap<String, String>();
        private static  Map<String,Boolean> existingTitle = new HashMap<String,Boolean>();
        private static String TitlesSha= INDEX_DIR+"/zwi_titles_sha1.ser"; 
	private static int Ncache=0;
	private static int isUpdate = 0; // no update
	private static long nr_articles=0;
	private static long nr_categories=0;
	private static int nr_updated=0;
        private static List<String> removedZWI = new ArrayList<String>(); // list with removed ZWI

	private static final  int  MaxToProcess=-1; // max number to process. Set to -1 for all


	private static  String removeScriptTag(String data){

		return  data.replaceAll("(?s)<script>.*?</script>", " ");

	}


	private static Pattern regex = Pattern.compile("\\[\\[Category:(.*?)\\]\\]");


        // get proper time
        public static String getTime(int seconds){

                LocalDateTime dateTime = LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
                String formattedDate = dateTime.format(formatter);
                return formattedDate;
        }


	/// Main. Pass 1 to update the index.
	public static void main(String[] args) throws Exception
	{


	        TimeZone.setDefault( TimeZone.getTimeZone("UTC"));
		long start = System.currentTimeMillis();
		Instant instant = Instant.now();
		timeStampSeconds = instant.getEpochSecond();


		isUpdate=0;
		if (args.length ==2) {


			try {
				isUpdate = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("Argument" + args[0] + " must be an integer. 0 for create, 1 for update.");
				System.exit(1);
			}

			INDEX_DIR = (args[1]).trim();


		} else {

			System.out.println("Wrong Nr of arguments. 0/1 (create or update) and the full path to index directory");
			System.exit(0);

		}


	        TitlesSha=INDEX_DIR+"/zwi_titles_sha1.ser";



                long previousProcessing=0;
                // previous settings
                if (isUpdate == 1) {
                        System.out.println("####  Index update mode! #### " );
                        File configFile = new File( confile );
                        System.out.println("Reading for update="+ confile );
                        try {
                                FileReader reader = new FileReader(configFile);
                                Properties props = new Properties();
                                props.load(reader);

                                String NR = props.getProperty("nr_articles");
                                System.out.println(" Nr of previous articles: " + NR);

                                String NC = props.getProperty("nr_categories");
                                System.out.println(" Nr of previous categories: " + NC);

                                String sT= props.getProperty("timestamp");
                                previousProcessing=Long.parseLong(sT);
                                Date date = new Date(previousProcessing * 1000);
                                DateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss zzz");
                                System.out.println(" Processed on: " + sT+" "+(df.format(date)).toString() );

                                //nr_articles  = Integer.parseInt(NR);
                                //nr_categories = Integer.parseInt(NC);


                                reader.close();
                        } catch (FileNotFoundException ex) {
                                // file does not exist
                        } catch (IOException ex) {
                                // I/O error
                        }


                     // read articles to be removed. Collect hashes of such articles 
		     removedZWI = new ArrayList<String>();
	             final String articles4remove= ZWI_PATH + "/index_removed.d";  
		     File f = new File(articles4remove);
                     if(f.exists() && !f.isDirectory()) { 
                     System.out.println("Removal taken from:" + articles4remove);
     		     try {
			Scanner scanner = new Scanner(f);
			while (scanner.hasNextLine()) {
                                String xline=scanner.nextLine();
				String[] arrOfStr = xline.split("\\|");
                                if (arrOfStr.length !=4) continue; // time, hash, publisher, title 
                                //System.out.println(arrOfStr.length);
                                System.out.println("Article to remove:"+arrOfStr[3]+" hash:"+arrOfStr[1]);  
				removedZWI.add(arrOfStr[1]); // record hash only 	
			}
			scanner.close();
		        } catch (FileNotFoundException e) {
			e.printStackTrace();
		        }	
                        System.out.println("Nr of files to remove:" + Long.toString(removedZWI.size()) );
		     } // end file exists 






                } // end update




                // read SQL index file //
		List<String> pathZWI = new ArrayList<String>();
                List<Long> idZWI = new ArrayList<Long>();
                List<String> timeZWI = new ArrayList<String>();
                List<String> sizeZWI = new ArrayList<String>();
                List<String> hashZWI = new ArrayList<String>();
	
                try {
                        final String TABLE_NAME="dbase_zwi";
                        System.out.println("Process="+ZWI_INDEX+"  table="+TABLE_NAME);
                        File dbFile = new File(ZWI_INDEX);
                        SqlJetDb db = SqlJetDb.open(dbFile,false);
                        ISqlJetTable table = db.getTable(TABLE_NAME);
                        db.beginTransaction(SqlJetTransactionMode.READ_ONLY);
                        ISqlJetCursor cursor=table.order(table.getPrimaryKeyIndexName());
                        int nnn=0;
                        try {
                                try {
                                        if (!cursor.eof()) {
                                                do {
                                                        long inx=cursor.getRowId();
                                                        long id=(Long)cursor.getInteger("rowid");
                                                        String s0=cursor.getString("path");
                                                        String s1=cursor.getString("title");
                                                        String s2=cursor.getString("timestamp");
							String s3=cursor.getString("filesize");
						        String s4=cursor.getString("hash"); // similar to ID but hash 

							// when updating, take only recent articles
							if (isUpdate == 1) {
                                                           long articletime=Long.parseLong( s2 );
                                                           long timediff=previousProcessing - articletime;
							   if (timediff>MaxTime) continue;
                                                         };


							s1=s1.trim();
                                                        s0=s0.trim();
                                                        if (s1.length()<1 || s0.length()<1) continue;
                                                        //System.out.println(Long.toString(id)+" "+s0+" "+s2+" "+s3);
                                                        pathZWI.add(s0);
							idZWI.add(id);
							timeZWI.add(s2);
							sizeZWI.add(s3);
                                                        hashZWI.add(s4);
							//System.out.println(s1+":"+s2);
                                                        //if (nnn%1000 ==0) System.out.println( Long.toString(nn)+" done");
                                                        nnn++;
                                                } while(cursor.next());
                                        }
                                } finally {
                                        cursor.close(); };
                        } finally {
                                db.commit();
                        }
                        System.out.println(Long.toString(nnn)+" articles processed from SQLite3 index");
                        db.close();
                } catch (Exception e) {
                        System.out.println(e.toString());
                }



		if (isUpdate == 0)  {
			System.out.println("####  Index create mode! #### " );
		};




		// create writer
		IndexWriter writer = createWriter();
		long nn=0;

		if (isUpdate == 1)
		  System.out.println("Consider Nr of recent articles:"+String.valueOf(pathZWI.size()) );


		for (int i = 0; i < pathZWI.size(); i++) {
                        nn++;

			if (isUpdate == 1) {
				// we only update articleis  erlier than MaxTime
				long file_time=Long.parseLong(  (String)timeZWI.get(i)   );
				long timediff=timeStampSeconds - file_time;
				if (timediff>MaxTime) continue;
			};


			Long fID=(Long)idZWI.get(i); // ID from SQL 
			String xff=(String)pathZWI.get(i)+".zwi";
                        String hash=(String)hashZWI.get(i);

                        //System.out.println(xff);
                        //if (xff.length()>1) continue; 
			//if (xff.indexOf("mythica")== -1) continue; 

			String zwifile_full=ZWI_PATH+xff;
			//System.out.println(zwifile_full);
                      
		        int    boost=9; // 100% boost, 0 lowest, 9 higest 	
			String metadata="";
			String article_html="";
			String publisher="";
			String title="";  // full title 
                        String stitle=""; // short title 
			String sha1="";
			String time_created="2022-01-01";
			String namespace="";
			String topics="";
			String categories="";
			String authors="";
			String rating="";
                        String lang="";
			String url="";
			String license="CC BY-SA 3.0";
			String zwifile=xff;
			String zwifile_key = hash; // zwifile.replaceFirst(".zwi",""); // without extension zwi
			Path p = Paths.get(xff);
			String short_zwi_name=p.getFileName().toString();
			short_zwi_name=short_zwi_name.replaceAll(".zwi","");
			//System.out.println(short_zwi_name);

			//System.out.println(zwifile_key);

			//System.out.println(zwifile);

			// skip trash 
			if (zwifile.indexOf("trash/")>-1) {
			         System.out.println("Skip trash article:"+zwifile);	
				 continue; 
                                 }

                        /* Debug
			boolean take=false; 
                        if (zwifile_full.indexOf("britannica")>-1) take=true;
		        if (zwifile_full.indexOf("sandbox")>-1) take=true;
                        if (take==false) continue;
                        */


			/*
			String article="";
			try {
			ZipFile zipFile = new ZipFile( zwifile_full ); 
			metadata=encycloindex.SetEnv.getStringFromZip("metadata.json", zipFile); 
			article_html=encycloindex.SetEnv.getStringFromZip("article.html", zipFile);    
		} catch (IOException e) {
			                    e.printStackTrace();
			            }

			 */


			String linux_cmd="unzip -p "+zwifile_full+" article.txt metadata.json";
			Process process = Runtime.getRuntime().exec( linux_cmd );
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder output = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
			String article= output.toString();
			reader.close();
			reader=null;

			//System.out.println(xff+" "+article);

                        String description="";
			//System.out.println(zwifile);
			int indxJason=article.lastIndexOf("\"ZWIversion");
			if (indxJason>0 && indxJason+8<article.length()){
				article_html=article.substring(0, indxJason-6);
				metadata=article.substring(indxJason-6,article.length());
				//System.out.println(metadata);

                                JSONObject jObject = null;
				try {
         				 jObject = new JSONObject(metadata);
				} catch (JSONException e) {
                                          System.err.println("   -> Error in "+ zwifile_full);
                                          System.err.println(e.toString());
                                          continue;
                                 }


                         
			        publisher="sandbox";
				if (jObject.has("Publisher")) { 
                                    try {	
                			    publisher=jObject.getString("Publisher");
                                            publisher=publisher.toLowerCase(); 
                                    } catch (JSONException e) {
                                                 System.err.println("   -> Error in "+ zwifile_full);
                                                 System.err.println(e.toString());
                                           continue;
                                         }
 
				};



                                lang="en";
                                if (jObject.has("Lang")) {
                                    try {
                                            lang=jObject.getString("Lang");
                                            lang=lang.toLowerCase();
                                    } catch (JSONException e) {
                                                 System.err.println("   -> Error in "+ zwifile_full);
                                                 System.err.println(e.toString());
                                           continue;
                                         }
                                };



                                // full title 
				if (jObject.has("Title"))
				        title=(jObject.getString("Title")).trim();

				// the baseline is to use long titles
				stitle=title;
                                if (jObject.has("ShortTitle"))
                                        stitle=(jObject.getString("ShortTitle")).trim();
                                        if (stitle.length()<1) stitle=title; // too short or not filled 

                                 // boost short titles. We decrease boost if (...)  
				 if (title.indexOf("(")>-1 && stitle.length()>0) {
                                         boost=8;
				 };
                                 // lower boost for britannica11  
                                 if (title.indexOf("britannica11")>-1) {
                                         boost=6;
                                 };
 
				if (jObject.has("TimeCreated")) { 
					try {
					   String ta=jObject.getString("TimeCreated");
                                           if (ta.length()>4) {
					
					   int foo= 1660947141;
					   try {
                                           foo = Integer.parseInt(ta);
                                           }
                                           catch (NumberFormatException e) {
                                           foo =  1660947141;
                                           }	   
					   time_created=getTime( foo );
					   } 
					} catch (JSONException e) {
                                                 System.err.println("   -> Error in "+ zwifile_full); 
      						 System.err.println(e.toString());
					 }
				};



                                // for historic encyclopedias, take proper publication date
                                if (jObject.has("PublicationDate")) {
                                        try {
                                           String tda=jObject.getString("PublicationDate");
                                           if (tda.length()>3) time_created=tda;
					} catch (JSONException e) {
                                                 System.err.println("   -> Error in "+ zwifile_full);
                                                 System.err.println(e.toString());
                                         }
                                };



                                /*
                                time_modified="0";
				if (jObject.has("LastModified")) { 
				           try {	  
				        	time_modified=jObject.getString("LastModified");
                                           } catch (JSONException e) {
                                                System.err.println("   -> Error in "+ zwifile_full);
  	        				System.err.println(e.toString());
                                                continue; 
                                         }
                                };
                                */


				license="GPL 3.0";
				if (jObject.has("License"))
					      license=jObject.getString("License");


				if (jObject.has("Description")) { 
				      try { 
				  	description=jObject.getString("Description");
                                      } catch (JSONException e) {
                                          System.err.println("   -> Error in "+ zwifile_full);
   	     			          System.err.println(e.toString());
                                          continue;
                                      }
                                 };
                              

				url=""; 
				if (jObject.has("SourceURL")) { 
                                        try {
                    	                	url=jObject.getString("SourceURL");
                                                url = url.replaceAll("https://en.citizendium.org", "https://citizendium.org");
                                         } catch (JSONException e) {
                                             System.err.println("   -> Error in "+ zwifile_full);
   	       				     System.err.println(e.toString());
                                             continue;
                                      }
					//System.out.println(url);
  				};

				/*
				// get sha1
				JSONObject resultObj = jObject.getJSONObject("Content");

				sha1 ="0";
                                try {
				     sha1 = resultObj.getString("article.txt");
				} catch (JSONException e) {
                                        System.err.println("   -> Error in "+ zwifile_full);
   					System.err.println(e.toString());
                                         continue;
                                 }
                                 */

				  // we get sha1 of the metadata since it check rating and the content! 
                                  sha1=SetEnv.getSHA(metadata);  
                                  //System.out.println(sha1);

                            
				JSONArray acategories=new JSONArray();
				if (jObject.has("Categories"))
					   acategories = jObject.getJSONArray("Categories");

				JSONArray atopics=new JSONArray();
				if (jObject.has("Topics"))
				       atopics = jObject.getJSONArray("Topics");


                                

				JSONArray lauthors = new JSONArray();
				if (jObject.has("ContributorNames")){	
                                             try { 
  					         lauthors=jObject.getJSONArray("ContributorNames");
                                             } catch(org.json.JSONException e ){
                                                 System.err.println("   -> Error in "+ zwifile_full);
    						 System.err.println(e.toString());
                                                 System.err.println("Continue.."); 
					     };
                                };


				 double drating=0;
                                // extract rating (scores,hits) 
                                JSONArray lrating = new JSONArray();
                                if (jObject.has("Rating")){
                                             try {
                                                 lrating=jObject.getJSONArray("Rating");

                                                 if (lrating.length()>1){
                                                     int Iscore=lrating.getInt(0);
                                                     int Ihits=lrating.getInt(1);
                                                     if (Ihits>2)
							      drating=Iscore/(double)Ihits;
						 }

					     } catch(org.json.JSONException e ){
                                                 System.err.println("   -> Error in "+ zwifile_full);
                                                 System.err.println(e.toString());
                                                 System.err.println("Continue..");
                                             };
                                };

                               
				if (drating>5){
                                         System.err.println("   -> BAD RATING (ABOVE 5). Skip. ");
                                         continue; 
				}
                           
			       // rating	
			       rating=String.format("%4.2f",  drating );	

				for (int j = 0; j<acategories.length(); j++) {
					if (j==0) categories=acategories.getString(j);
					else categories=categories+";"+acategories.getString(j);
					nr_categories++;
				}
				for (int j = 0; j<atopics.length(); j++) {
					if (j==0) topics=atopics.getString(j);
					else  topics=topics+";"+atopics.getString(j);
				}

				for (int j = 0; j<lauthors.length(); j++) {
					if (j==0) authors=lauthors.getString(j);
					else  authors=authors+";"+lauthors.getString(j);
				}


				//System.out.println(sha1);
			};


        		//System.out.println(xff+" "+article_html);


			// get body
			//String txt = SetEnv.getCleanTXT(article_html);
			String txt = article_html; // already in txt
                        // replace nelines with spaces
                        txt = txt.replaceAll("[\\t\\n\\r]+"," "); // replace new lines  
                        txt = txt.replaceAll("\\s+", " ");        // replace multiple spaces with 1 


			// remove short description
			txt=txt.replaceAll("Short description:"," ");
			txt=txt.replaceAll("short description:"," ");
                        txt=txt.trim();


                        String article_key  = publisher+"|"+title;
                        boolean vote = true;

                        //System.out.println(xff+" "+txt);


			// too short to consider
			if (txt.length()<20) continue;

			// full title acts as URL, so it should corresponds to ZWI file name without path
			String full_path=short_zwi_name.trim();

			//System.out.println(full_path);

			// remove namespace
			String[] arrOfStr = title.split(":",2);
			if (arrOfStr.length>1) title=arrOfStr[1];
                        stitle=stitle.replaceAll("_"," "); // just in case 
                        title=title.replaceAll("_"," "); // just in case 

			/*
			int  itime_created=0;
			int  itime_modified=0;
			try{
				itime_created = Integer.parseInt(time_created);
				itime_modified = Integer.parseInt(time_modified);
			} catch (NumberFormatException ex){
                                System.out.println("   -> Error in time formatting. Continue..");
				continue;
				//ex.printStackTrace();
			}
                        */


			if (isUpdate == 0) {
				Document document1 = createDocument(fID,
				                                    hash, // same as ID but not numeric and fixed	
				                                    title,
								    stitle, // short title 
				                                    full_path,
				                                    publisher,
								    lang, 
								    url,
				                                    txt,
	                                                            description,	
								    topics,
				                                    categories,
				                                    time_created,
				                                    authors,
				                                    sha1,
				                                    rating,
				                                    license, 
								    zwifile,
								    boost);

				existingTitleSha1.put(zwifile_key, sha1); // add to map
                                existingTitle.put(article_key,vote); // add to map

				writer.addDocument(document1);
				if (nn%100 ==0) System.out.println(Long.toString(nn)+" "+title+ " ["+full_path+"]");
			};



			// update the index
			if (isUpdate == 1)  {
				if (existingTitleSha1.containsKey( zwifile_key  ) == false)  {

					nr_articles++;
                                        nr_updated++;
					Document document1 = createDocument(fID,
							                    hash,  
					                                    title,
									    stitle, // short title  
					                                    full_path,
					                                    publisher,
									    lang, 
									    url,
					                                    txt,
									    description, 
					                                    topics,
					                                    categories,
					                                    time_created,
					                                    authors,
					                                    sha1,
					                                    rating,
					                                    license, 
									    zwifile,
									    boost);
					writer.addDocument(document1);
					// add new article at position nr_articles++
					System.out.println(Long.toString(nr_articles)+"  -> New article added: "+full_path);

					existingTitleSha1.put(zwifile_key, sha1); // add to map
                                        existingTitle.put(article_key, vote); // add to map

				} else {
					// if exists, check sha1 that update if needed
					String oldsha1=existingTitleSha1.get( zwifile_key  );
					if (oldsha1.equals( sha1 ) == false) {

      						nr_articles++;
      				                Document document1 = createDocument(fID,
						                                    hash,	
						                                    title,
										    stitle, // short title  
						                                    full_path,
						                                    publisher,
										    lang, 
										    url,
						                                    txt,
										    description, 
						                                    topics,
						                                    categories,
						                                    time_created,
						                                    authors,
						                                    sha1,
						                                    rating,
						                                    license, 
										    zwifile,
										    boost);

						writer.updateDocument( new Term("sha1",sha1), document1 );
						System.out.println(Long.toString(nn)+" -> "+full_path+" updated");

						existingTitleSha1.put(zwifile_key, sha1); // add to map
                                                existingTitle.put(article_key,vote); // add to map

						nr_updated++;
					} else {
						//System.out.println("No update needed for "+key);
					};
				};
			}


		} // end loop




                  // now we remove some documents in update mode 
                  if (isUpdate ==1) {
                        for (int indx = 0; indx < removedZWI.size(); indx++) {
                          String hasRemove=(String)removedZWI.get( indx );
                          Term term = new Term("hash",hasRemove);
                          writer.deleteDocuments(term);
                          String ss=Integer.toString(indx); 
			  System.out.println("Remove article: ["+ss+"] "+hasRemove+" from index");  
			}
                    };



		nr_articles=nn;
		writer.commit();
		writer.close();

		System.out.println(" -> "+Long.toString(nr_articles)+" articles");

		System.out.println(" -> Write config: "+ confile );
		File configFile = new File( confile );
		try {

			// write map with titles and sha1 for speed update
			System.out.println("Write:"+ TitlesSha );
			FileOutputStream     fos = new FileOutputStream( TitlesSha );
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(existingTitleSha1);
			oos.close();
                        fos.close();

                        // write map with titles and votes for general use 
                        System.out.println("Write:"+INDEX_DIR+"/zwi_titles.txt" );
                        FileOutputStream fos2 = new FileOutputStream( INDEX_DIR+"/zwi_titles.txt");
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos2));
                        for (Map.Entry<String,Boolean> entry : existingTitle.entrySet()) { 
                                bw.write( entry.getKey() );
                                bw.newLine();
                        }
	                bw.close();
                        fos2.close();

			// also write some properties
			Properties props = new Properties();
			props.setProperty("nr_articles",  Long.toString(nr_articles));
                        props.setProperty("nr_articles_updated",  Long.toString(nr_updated));
			props.setProperty("nr_categories", Long.toString(nr_categories ));
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
			Date now = new Date();
			String strDate = sdfDate.format(now);
			//props.setProperty("timestamp",  strDate );
                        long secnow=(long)(java.time.Instant.now().getEpochSecond()); 
		        props.setProperty("timestamp",  String.valueOf(secnow)  );	
			FileWriter writerfile = new FileWriter(configFile);
			props.store(writerfile, "search settings");
			writerfile.close();
		} catch (FileNotFoundException ex) {
			// file does not exist
		} catch (IOException ex) {
			// I/O error
		}


		System.out.println("Total documents processed="+Long.toString(nn));
		if (isUpdate==1) System.out.println("Documents updated="+Integer.toString(nr_updated));



		System.out.println(" Index took ="+ Integer.toString( (int)( (System.currentTimeMillis() - start)/1000. ) )+" sec" );

	}



	// create document
	private static Document createDocument(long rowid,
		                               String hash,
	                                       String title,
					       String stitle, 
	                                       String full_path,
	                                       String publisher,
                                               String lang,
					       String url,
					       String content,
					       String description, 
	                                       String topics,
	                                       String categories,
	                                       String time_created,
	                                       String authors,
	                                       String page_sha1,
	                                       String rating,
	                                       String license,
	                                       String zwifile,
					       int value) { 
	
		Document document = new Document();

		if (topics == null) topics="";
	        if (topics == "none") topics="";


                // split using # since we want last
		// we do not use last # since the new file convention 
	        String[] parts = full_path.split("#");	
	        if ( parts.length>1)
		                  full_path=parts[parts.length-1]; // last part
                //System.out.println(full_path);
                //System.out.println(zwifile);
		//Note: redirect have ns= -1!

		// do not tokanize
		//document.add(new LongPoint("rowid", rowid));
		//document.add(new IntPoint("time_created", time_created));
		//document.add(new IntPoint("time_modified", time_modified));

		document.add(new StringField("rowid",  Long.toString(rowid), Field.Store.YES));
                document.add(new StringField("hash",  hash, Field.Store.YES));
		document.add(new StringField("time_created", time_created, Field.Store.YES));
		document.add(new StringField("sha1",  page_sha1, Field.Store.YES));
		document.add(new StringField("full_path", full_path, Field.Store.YES));
                document.add(new StringField("url", url, Field.Store.YES));
		document.add(new StringField("publisher",  publisher, Field.Store.YES));
                document.add(new StringField("lang",  lang, Field.Store.YES));
		document.add(new StringField("rating",  rating, Field.Store.YES));
		document.add(new StringField("license",  license, Field.Store.YES));
                document.add(new StringField("description", SetEnv.removeTags(description), Field.Store.YES));
		document.add(new StringField("zwifile",  zwifile, Field.Store.YES));
            
		// tokanize
                document.add(new TextField("stitle", stitle , Field.Store.YES)); // short title 
		document.add(new TextField("title", title , Field.Store.YES));
		document.add(new TextField("title_w", title , Field.Store.YES));
		//document.add(new TextField("title_k", title , Field.Store.YES));

		document.add(new TextField("content", content, Field.Store.YES));
		document.add(new TextField("content_w", content, Field.Store.YES));

		document.add(new TextField("categories",  categories, Field.Store.YES));
		document.add(new TextField("authors",  authors, Field.Store.YES));
		document.add(new TextField("topics",  topics, Field.Store.YES));

		// https://lucene.apache.org/core/8_0_0/core/org/apache/lucene/document/SortedNumericDocValuesField.html
		// boost results (default is 100L) 
          	//document.add( new SortedNumericDocValuesField( "boost", value) );
	        // boost. 100 is maximum boost. Lower is less boosted.	
                document.add (new StringField("boost", Integer.toString(value), Field.Store.YES));

                //System.out.println(Integer.toString(value));


		return document;
	}








	private static IndexWriter createWriter() throws IOException
	{

		boolean create=true;
		if (isUpdate ==1) {
			create=false;

			File outputDir = new File( INDEX_DIR );
			Directory Mindex = FSDirectory.open(outputDir.toPath());
			IndexReader reader = DirectoryReader.open(Mindex);
			IndexSearcher searcher = new IndexSearcher(reader);

			nr_articles = reader.numDocs();
			long  version = ((DirectoryReader) searcher.getIndexReader()).getVersion();
			System.out.println("Lucene Version="+Long.toString(version) );
			System.out.println("List with sha1 Map: Read:"+ TitlesSha );
			try {
				FileInputStream fis = new FileInputStream( TitlesSha );
				ObjectInputStream ois = new ObjectInputStream(fis);
				existingTitleSha1  = (Map<String,String>) ois.readObject();
				ois.close();


                         System.out.println("List with titles:"+INDEX_DIR+"/zwi_titles.txt" );
                         File file = new File( INDEX_DIR+"/zwi_titles.txt" );
                         try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                         String line;
                         while ((line = br.readLine()) != null) {
                                  existingTitle.put(line.trim(),true); 
                            }
                          }



			} catch (FileNotFoundException e) {e.printStackTrace();
			} catch (IOException e) {e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}







			/*
			nr_articles=0;
			for(int i=0; i<reader.numDocs(); i++) {
								 Document  doc =  reader.document(i);
			                                    if (doc == null) continue; 

			    				         String stime = doc.get("time_modified"); 
			                long file_time=Long.parseLong(stime);
			                                            long timediff=timeStampSeconds - file_time;
			                                            if (timediff>MaxTime) continue;

			               				 String sha1 = doc.get("sha1"); 
			                                            String full_path= doc.get("full_path");
			                                            String publisher= doc.get("publisher");
			         //System.out.print(i); 
				 //System.out.println(" "+sha);
			               				 existingTitle.put(publisher+":"+full_path, sha1);
			 					         if (i%1000 == 0) System.out.println(Integer.toString(i)+ " fill sha1 map..");
			                                            nr_articles++;
		}

			                            */
			System.out.println("####  READ LUCENE INDEX ="+ INDEX_DIR+" Nr documents="+Long.toString(nr_articles));
			System.out.println("####  MAP with sha1 hash is made  ="+Integer.toString(existingTitleSha1.size()) );

			reader.close();
		};
		System.out.println("####  LUCENE INDEX ="+ INDEX_DIR);


		File outputDir = new File( INDEX_DIR );
		FSDirectory dir = FSDirectory.open( outputDir.toPath() );
		StandardAnalyzer stdAnalyzer = new StandardAnalyzer();


		Map<String,Analyzer> analyzerPerField = new HashMap<>();
		analyzerPerField.put("title_w", new WhitespaceAnalyzer());
		//analyzerPerField.put("title_k", new KeywordAnalyzer());
		analyzerPerField.put("title", stdAnalyzer);
                analyzerPerField.put("stitle", stdAnalyzer);

		analyzerPerField.put("content_w", new WhitespaceAnalyzer());
		analyzerPerField.put("content", stdAnalyzer);

		analyzerPerField.put("category", new KeywordAnalyzer());
		analyzerPerField.put("topic",  new KeywordAnalyzer());
		analyzerPerField.put("authors", stdAnalyzer);


		PerFieldAnalyzerWrapper aWrapper =
		    new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerPerField);

		System.out.println("Write index file..");
		IndexWriterConfig config = new IndexWriterConfig(aWrapper);

		//config.setCodec( new Lucene87Codec(  Lucene87Codec.Mode.BEST_COMPRESSION ) );
		//config.setCodec( new Lucene87Codec(  Lucene87Codec.Mode.BEST_SPEED ) );

		//config.setMaxBufferedDocs(1); // only one segment is enough

		// default before Sep 2022
                final double DEFAULT_RAM_BUFFER_SIZE_MB_STORE = 2048.;
		//config.setMaxBufferedDocs(50000);
		config.setRAMBufferSizeMB( DEFAULT_RAM_BUFFER_SIZE_MB_STORE );

		config.setSimilarity( new ClassicSimilarity() );

		if (isUpdate ==0) config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);// overwrites
		if (isUpdate ==1) config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);// overwrites

		// if
		// needed
		IndexWriter writer = new IndexWriter(dir, config);

		return writer;
	}


}
