package encyclofind;

import java.io.*;
import java.util.*;
import java.util.regex.*;


public final class SetEnv {


	static public String[] common = {"class", "public", "private", "protected", "method", "the","of","and","a","to","in","is","you","that","it","he","was","for","on","are","as","with","his","they","I","at","be","this","have","from","or","one","had","by","word","but","not","what","all","were","we","when","your","can","said","there","use","an","each","which","she","do","how","their","if","will","up","other","about","out","many","then","them","these","so","some","her","would","make","like","him","into","time","has","look","two","more","write","go","see","no","way","could","my","than","been","who","oil","its","now","down","did","get"};


	private final static Pattern WB_PATTERN = Pattern.compile("(?<=\\w)\\b");


	static public String removeScriptContent(String filename) {

		String tmp="";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			tmp  = sb.toString();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();

		}

		return tmp;

	}



    // check if text has a capital latter or digit 
    public  static boolean checkCapitalOrDigit(String str) {
    char ch;
    boolean capitalFlag = false;
    boolean numberFlag = false;
    for(int i=0;i < str.length();i++) {
        ch = str.charAt(i);
        if( Character.isDigit(ch)) {
            numberFlag = true;
        }
        if (Character.isUpperCase(ch)) {
            capitalFlag = true;
        }
    }

    if(numberFlag || capitalFlag) return true;

    return false;
    }



	/**
	 * Get text between two strings. Passed limiting strings are not 
	 * included into result.
	 *
	 * @param text     Text to search in.
	 * @param textFrom Text to start cutting from (exclusive).
	 * @param textTo   Text to stop cuutting at (exclusive).
	 */
	public static String getBetweenStrings(
	    String text,
	    String textFrom,
	    String textTo) {

		String tmp=text;
		int ind1=text.indexOf(textFrom);
		int ind2=text.lastIndexOf(textTo);

		int len=textFrom.length();

		//System.out.println("Start="+textFrom+ " End="+textTo);
		//System.out.println(ind2);
		//System.out.println(ind1+len);

		if (ind2>ind1+len)
			tmp=text.substring(ind1+len,ind2);

		return tmp;

		//  return StringUtils.substringBetween(text, textFrom, textTo);

	}


	/*
	// get a part of first sentance
	public static String getSentence(String txt){

		final int Mx=100; // max substring
		if (txt==null) return "";
		if (txt.length()<Mx) return "";

		txt=txt.substring(0,Mx);
		txt=txt.replaceAll("\\|"," ");

		txt=txt.trim();
		if (txt==null) return "";
		if (txt.length()<3) return "";
		String app="";
		int n=0;
		int MaxCat=10;
		String categories="";

		String[] arrSplit = txt.split(" ");
		for (String a : arrSplit) {
			a=a.trim();
			if (a.equals("*") == true) continue;
			categories=categories+" "+a;
			n++;
			if (n>MaxCat){
				app = "...";
				break;
			};
		};

		categories=categories+app;
		return categories;
	};
        */



public static String getSentence(String s) {

   int n=40; // max number of words   
   if (s == null) return null;
   if (n <= 0) return "";
   Matcher m = WB_PATTERN.matcher(s);
   for (int i=0; i<n && m.find(); i++);
   if (m.hitEnd()) { 
           if (s.endsWith(".")==false) return s +"."; 	
	   return s;
   }   else
      return s.substring(0, m.end())+" ...";
   }


}
