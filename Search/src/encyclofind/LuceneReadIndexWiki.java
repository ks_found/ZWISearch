package encyclofind;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
//import org.apache.lucene.analysis.standard.ClassicAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.search.*;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.*;
import org.apache.lucene.queryparser.classic.*;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenSources;
//import org.apache.lucene.search.spans.SpanQuery; 
//import org.apache.lucene.search.spans.SpanMultiTermQueryWrapper;
//import org.apache.lucene.search.spans.SpanNearQuery;
//import org.apache.lucene.search.spans.SpanFirstQuery;
import org.apache.lucene.queryparser.complexPhrase.ComplexPhraseQueryParser;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.time.ZoneOffset;
import java.time.Instant;
import java.util.Scanner;

// https://howtodoinjava.com/lucene/lucene-search-highlight-example
//
public class LuceneReadIndexWiki
{

	private static String INDEX_DIR;
	private static String TYPE_WIKI="<img src=\"img/wiki.png\" style=\"vertical-align:middle;margin:0;auto;\"/>";
	private static String TYPE_SEARCH="<img src=\"img/search.png\"/>";
        // time of previous look-up
	private static long oldTimestamp = 0; 
         // list with offline encyclopedias
        private static ArrayList<String> offlineEncyclopedia = new ArrayList<String>();
        /**
        * Get image depending on publisher 
	* Input: publisher
        * @return image 
        * **/	
	private static String getWIKI(String newurl){
		// postfix. Need to find a bug
                TYPE_WIKI="<img src=\"img/sandbox_s.png\" title=\"SandBox\"/>";

		//System.out.println(newurl);

		if (newurl.indexOf("handwiki")>-1) { 
			TYPE_WIKI="<img src=\"img/handwiki_s.png\" title=\"HandWiki\"/>";
                        return TYPE_WIKI;
		} else if (newurl.indexOf("wikipedia")>-1) { 
			TYPE_WIKI="<img src=\"img/wikipedia_s.png\" title=\"Wikipeda\"/>";
                        int i1=newurl.indexOf("wikipedia_"); 
                        //System.out.println(newurl.length());
			int i2=i1+10+2; 
		        if (i1>-1 && newurl.length() == i2) { 
				 String lang=newurl.substring(i1+10,i2);
                                 //System.out.println(lang); 
       				 TYPE_WIKI="<img src=\"img/wikipedia_"+lang+"_s.png\" title=\"Wikipeda_" + lang+"\"/>";
				 } 
                         return TYPE_WIKI;
		} else if (newurl.indexOf("citizendium")>-1)
			TYPE_WIKI="<img src=\"img/citizendium_s.png\" title=\"Citizendium\"/>";
		else if (newurl.indexOf("encyclopediaofmath")>-1)
			TYPE_WIKI="<img src=\"img/eom_s.png\" title=\"Encyclopedia of Mathematics\"/>";
		else if (newurl.indexOf("wikitia")>-1)
			TYPE_WIKI="<img src=\"img/wikitia_s.png\" title=\"Wikitia\"/>";
		else if (newurl.indexOf("scholarpedia")>-1)
			TYPE_WIKI="<img src=\"img/scholarpedia_s.png\" title=\"Scholarpedia\"/>";
		else if (newurl.indexOf("edutechwiki")>-1)
			TYPE_WIKI="<img src=\"img/edutech_s.png\" title=\"Edutechwiki\"/>";
		else if (newurl.indexOf("ballotpedia")>-1)
			TYPE_WIKI="<img src=\"img/ballotpedia_s.png\" title=\"Ballotpedia\"/>";
		else if (newurl.indexOf("eol.org")>-1)
			TYPE_WIKI="<img src=\"img/eol_s.png\" title=\"EoL\"/>";
		else if (newurl.indexOf("sep")>-1)
			TYPE_WIKI="<img src=\"img/sep_s.png\" title=\"SEP\"/>";
                else if (newurl.indexOf("mythica")>-1)
                        TYPE_WIKI="<img src=\"img/encyclopedia_mythica_s.png\" title=\"Mythica\"/>";
                else if (newurl.indexOf("isbe")>-1)
                        TYPE_WIKI="<img src=\"img/isbe_s.png\" title=\"Bible Encyclopedia\"/>";
                else if (newurl.indexOf("whe")>-1)
                        TYPE_WIKI="<img src=\"img/whe_s.png\" title=\"World History Encyclopedia\"/>";
                else if (newurl.indexOf("microwiki")>-1)
                        TYPE_WIKI="<img src=\"img/microwiki_s.png\" title=\"Micronations\"/>";
                else if (newurl.indexOf("britannica11")>-1)
                        TYPE_WIKI="<img src=\"img/britannica11_s.png\" title=\"Britannica 11th\"/>";
                else if (newurl.indexOf("sandbox")>-1)
                        TYPE_WIKI="<img src=\"img/sandbox_s.png\" title=\"Sandbox\"/>";
                else if (newurl.indexOf("enhub")>-1)
                        TYPE_WIKI="<img src=\"img/enhub_s.png\" title=\"EnHub\"/>";
                else if (newurl.indexOf("greatplains")>-1)
                        TYPE_WIKI="<img src=\"img/greatplains_s.png\" title=\"Great Plains Encyclopedia\"/>";
                else if (newurl.indexOf("jewishenc")>-1)
                        TYPE_WIKI="<img src=\"img/jewishenc_s.png\" title=\"Jewish Encyclopedia\"/>";
                else if (newurl.indexOf("conservapedia")>-1)
                        TYPE_WIKI="<img src=\"img/conservapedia_s.png\" title=\"Conservapedia\"/>";
                else if (newurl.indexOf("rationalwiki")>-1)
                        TYPE_WIKI="<img src=\"img/rationalwiki_s.png\" title=\"Rationalwiki\"/>";
                else if (newurl.indexOf("mdwiki")>-1)
                       TYPE_WIKI="<img src=\"img/mdwiki_s.png\" title=\"MDwiki\"/>";
                else if (newurl.indexOf("nwe")>-1)
                        TYPE_WIKI="<img src=\"img/nwe_s.png\" title=\"NWE\"/>";
                else if (newurl.indexOf("wikiversity")>-1)
                        TYPE_WIKI="<img src=\"img/wikiversity_s.png\" title=\"Wikiversity\"/>";
                else if (newurl.indexOf("wikidoc")>-1)
                        TYPE_WIKI="<img src=\"img/wikidoc_s.png\" title=\"Wikidoc\"/>";
		return TYPE_WIKI;

	};


	// get URL from name and path
	// publish: publisher
	// path: full path to ZWI 
        // ext_url: External URL
	// mtype=10 for external (title,publisher), 11 (ID), mtype=0,1 - for encycloreader. 
        // isLocalZWI true, then  only show local ZWI
	private static String getEnURL(String publish, String path, String ext_url,String pid, int mtype,boolean isLocalZWI){

		 // only locally stored
		 if (isLocalZWI)  return "/db/view.php?id="+pid;


                // external URL links
                if (publish.indexOf("mythica")>-1) return ext_url;
		else if (publish.indexOf("whe")>-1) return ext_url;
                else if (publish.indexOf("greatplains")>-1) return ext_url;
                //else if (publish.indexOf("jewishenc")>-1) return ext_url;

		//if (publish.indexOf("isbe")>-1) return ext_url;
		else if (publish.indexOf("isbe")>-1) return "/db/view.php?id="+pid;
		else if (publish.indexOf("britannica11")>-1) return "/db/view.php?id="+pid;
		else if (publish.indexOf("sandbox")>-1) return "/db/view.php?id="+pid;
		else if (publish.indexOf("enhub")>-1) return "/db/view.php?id="+pid;
                else if (publish.indexOf("jewishenc")>-1) return "/db/view.php?id="+pid;
                else if (publish.indexOf("nwe")>-1) return "/db/view.php?id="+pid;


		// reconfigure to ZWI view for encyclopedias which are offline
                if (offlineEncyclopedia.size()>0) {
                    boolean takeInternal=false;
		    for (int i = 0; i < offlineEncyclopedia.size(); i++) {  
                       if (ext_url.indexOf((String)offlineEncyclopedia.get(i))>-1) takeInternal=true; 
		    }	
                    if (takeInternal) return "/db/view.php?id="+pid;
		}; 

		// internal URL links
		String tmp="/r/"+publish+".php?q="+path;
                if (mtype == 10) tmp="view.php?p="+publish+"&t="+path; 
                else if (mtype == 11) tmp="view.php?id="+pid;


		return tmp;
	};





	/**  
	 * Perform searches in titles.
	 * @param hits hits to be passed
	 * @param searcher current searcher
	 * @param Language language
	 * @param isLocalZWI should ZWI locally shown? 
         * @return string with the HTML results. 
	 *
	 **/
        private static String searchInTitles( ScoreDoc[]  hits,  IndexSearcher searcher,  int type, ArrayList<String> usedTitle, boolean isFirst, String Unselected, String Language,boolean isLocalZWI){

	        String RES="";
                int Istart=0;
                float maxScore=1.0f;
                int nn=0;

		for (int start = Istart; start < hits.length; start++) {
                        int docId = hits[start].doc;
        
                        Document d=null;	
                        try {	
			       d = searcher.doc(docId);
			} catch(IOException e) {
                               // e.printStackTrace();
                               continue;
			    }
                        if (d == null) continue;

        		String sID=d.get("hash"); //   d.get("rowid"); // Just ID of an article 
                        //long fID = Long.parseLong(sID);
                        String title=d.get("title"); // title without namespace
                        String stitle=d.get("stitle"); // short title 
                        //String lang=d.get("lang"); // short title
			String full_path=d.get("full_path");
                        String topics=d.get("topics");
                        String category=d.get("categories");
                        String publisher=d.get("publisher");
                        String description=d.get("description");
                        String ext_url=d.get("url");
                        String srating=d.get("rating");
 			try {
                             double rating = Double.parseDouble(srating);
                             if (rating>5) continue; 
                        } catch (NumberFormatException e) {};


                        /*
                        if (Language.length() ==2 ) {
                                                boolean takeLanguageSpecific=false;
                                                if (Language.equals(lang)) takeLanguageSpecific=true;
                                                if (takeLanguageSpecific==false) continue;
                        } else {}
                        */



			//String boost=d.get("boost");
                        //System.out.println(boost);

        		// if (publisher.indexOf("jewishenc")>-1) continue; 
				

                        //unselect publisher 
			//if (Unselected.indexOf(publisher)>-1) continue; 

			if (start == Istart)  maxScore=(hits[start].score);
                        String categories=getFormattedCategories(category, publisher);
                        String time=d.get("time_created");
                        if (title.length()<2) continue;
                        // no double counting
                        String sha1=d.get("sha1");
                        if (isFirst == false) 
				  if (usedTitle.contains( sha1 )) continue;
                        usedTitle.add( sha1 );
                        nn++;
                        int score= (int)( (hits[start].score / maxScore)*100);
                        String iscore=String.valueOf(score);
                        TYPE_WIKI=getWIKI(publisher);
                        String url=getEnURL(publisher,full_path,ext_url,sID,type,isLocalZWI);
                        String desc=getTopic(publisher, topics);
                        //String sentance=SetEnv.getSentence(d.get("content"));
                        String sentance=SetEnv.getSentence(  description );

			RES=RES+"<li><span class=\"xitem\">";
	                url = url.replaceAll("'","%27");	
                        //String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span> <span class=\"x2\"> "+ desc + "</span> "+ sentance+" <span style=\"color:#008000;font-size:80%;\"> "+time+"</span> "+categories;
                        String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span>: "+ sentance+" "+desc+" <span style=\"color:#008000;font-size:80%;\"> [" + iscore + "%] " +time+"</span> "+categories;

                        RES=RES+tmp+"</span></li>\n\n";
                } // end loops


               return RES;

        }


	// get a string that describes the text 
	private static String getTopic(String publish, String topic){
		//String wiki=publish.substring(0,1).toUpperCase() + publish.substring(1).toLowerCase();
		//String tmp=" [from <b>"+wiki+"</b>] " ;
		//if (topic != null)
	        //		if (topic.length()>2) tmp=tmp+"(in <b><i><a href='/r/"+publish+".php?q="+topic+"'>"+topic+"</a></i></b>) ";
                String tmp=" ";
		if (topic != null)
                       if (topic.length()>3) {
			         // take only first topic, ignore others        
			         String[] arrOfStr = topic.split(";");
                                 if (arrOfStr.length>1) tmp="(<i>"+arrOfStr[0]+"</i>) "; 
			         else tmp="(<i>"+topic+"</i>) ";		        
		       };
		return tmp;
	};


        // get a string that describes the publisher 
        private static String getPublish(String publish, String topic){
                String wiki=publish.substring(0,1).toUpperCase() + publish.substring(1).toLowerCase();
                String tmp=" [from <b>"+wiki+"</b>] " ;
                return tmp;
        };




	// when calling statically
	public static void main(String[] args) throws Exception
	{

		try {
			System.out.println(processSearch(args));
		}  catch (Exception e) { }

	};


	// get proper time
        /*
	public static String getTime(int seconds){

		LocalDateTime dateTime = LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		String formattedDate = dateTime.format(formatter);
		return formattedDate;
	}
        */


	// you can all it from a server too.
	public static String processSearch(String[] args) throws Exception {


		//System.out.println(args.length);
		if (args.length <8) {
			return "No search arguments! Should be: Start Hits Type INDEX_PATH Unselected Lang Option Word1 Word2 Word3 Word4 Word5";
			//System.exit(0);
		}
/*
        for (int index = 0; index < args.length; ++index)
        {
            System.out.println("args[" + index + "]: " + args[index]);
        }

*/
		//System.out.println(args.length);

		int Istart=Integer.parseInt( args[0].trim() );
		int Ihits=Integer.parseInt( args[1].trim() );


		// type=0 - title
		// type=1  -all
                // type=10 - all but for external usage using title and encyclopedia 
	        // type=11 - all but for external usage using ID  
	
		int type=Integer.parseInt( args[2].trim() );
		INDEX_DIR=args[3].trim();
                String Unselected=args[4].trim(); // string with unselected publishers;
                String Language=args[5].trim(); // string with language code. If 2-characters, use that code;
                String Option=args[6].trim(); // string with options;

		//System.out.println(Option);
                boolean isLocalZWI=false;
                if (Option.equals("loc")) isLocalZWI=true; 

		// array with unselected publishers
                String[] UnselectedParts = Unselected.split(",");


		// we allow up to 12 arguments
		String SEARCH=args[7].trim();
	        int maxWords=12;	
                for (int iii = 8; iii < args.length; iii++) {
                   SEARCH=SEARCH+" "+args[iii].trim();
                   if (iii>maxWords) break;
		}
                SEARCH=SEARCH.trim();


		 // check status of encyclopedias withing 5 min interval (300 sec) 
                 long unixTimestamp = Instant.now().getEpochSecond();
		 if ( (unixTimestamp - oldTimestamp)>300) {
                         offlineEncyclopedia.clear();
			 String bad_encyclopedia_list="/var/www/html/encycloreader/public_html/status/encyclopedias_down.txt";
			 File f = new File(bad_encyclopedia_list);
                         if (f.exists()) {
                         try {
			    Scanner scanner = new Scanner( f );
			    while (scanner.hasNextLine()) {
                                    String bad_encyclopedia= (scanner.nextLine()).trim();
                                    if (bad_encyclopedia.length()>2){
                                          offlineEncyclopedia.add(bad_encyclopedia); 
				    };
				    //System.out.println(scanner.nextLine());
			     }
			     scanner.close();
		             } catch (FileNotFoundException e) {
	              		//e.printStackTrace();
		             }
			 }
			 oldTimestamp=unixTimestamp;
                         //System.out.println(bad_encyclopedia_list);
			 //System.out.println(offlineEncyclopedia.size()); 
		 }	 


                //System.out.println(args.length);
		//System.out.println("Find="+SEARCH+ " Index="+INDEX_DIR);
		String[] SEARCH_WORDS = SEARCH.split(" ");


		String RES="";

		File outputDir = new File( INDEX_DIR );
		if (!outputDir.exists()) {
			return INDEX_DIR + " does not exists... Exit.";
		}

		Directory Mindex = FSDirectory.open(outputDir.toPath());
		IndexReader reader = DirectoryReader.open(Mindex);
		IndexSearcher searcher = new IndexSearcher(reader);
		searcher.setSimilarity(  new ClassicSimilarity() );

		WhitespaceAnalyzer analyzer_w = new WhitespaceAnalyzer();
		Analyzer analyzer = new StandardAnalyzer();

		// first try whitespace analysers
		PhraseQuery query_w = new PhraseQuery("title_w", SEARCH_WORDS);


	        //sorting according to boost. 9 is max, 0 is min
	        //Sort sort = new Sort(new SortField("boost", SortField.Type.STRING));

                //Sort sort = new Sort(new SortField("boost", SortField.Type.LONG, true));
                //return searcher.search(query, maxResults, sort);

		BooleanQuery.Builder bqt1 = new BooleanQuery.Builder();
		bqt1.add(query_w, Occur.MUST);

		// condition for unselecting publishers
		for (int iii = 0; iii < UnselectedParts.length; iii++) {
                        if (UnselectedParts[iii].length()>1) { 
			  Query unQuery = new TermQuery(new Term("publisher", UnselectedParts[iii]));
     		   	  bqt1.add(unQuery, Occur.MUST_NOT);
                        }
			}


                // use language constraint
	         if (Language.length() ==2 ) {	
                        Query langQuery = new TermQuery(new Term("lang", Language));
                        bqt1.add(langQuery, Occur.MUST);
                 };


		// with constraint
                BooleanQuery query_ww=bqt1.build();


		// no phrase
		//QueryParser parser_w = new QueryParser("title_w", analyzer_w);
		//Query query_w = parser_w.parse( SEARCH );
		TopDocs docs_w = searcher.search(query_ww, Istart+Ihits);
		ScoreDoc[] hits_w  = docs_w.scoreDocs;
		//System.out.println("Whitespace Results :: " + docs_w.totalHits);


		// start usual analyser
		// PhraseQuery query1 = new PhraseQuery("title", SEARCH_WORDS);
		// BooleanQuery.Builder bqt2 = new BooleanQuery.Builder();
		// bqt2.add(query1, Occur.SHOULD);


		// usual standard quary 
		QueryParser parser1 = new QueryParser("title", analyzer);
		parser1.setDefaultOperator(QueryParser.Operator.AND);
		parser1.setPhraseSlop(0); // exact phrase
                Query query1 = parser1.parse( SEARCH );
                if  (SEARCH.indexOf("?") >-1 &&  SEARCH.length()>2) {
                        query1 = new WildcardQuery(new Term("title", SEARCH));
		};


                // add constraints
                BooleanQuery.Builder bqt2 = new BooleanQuery.Builder();
                bqt2.add(query1, Occur.MUST);

                // condition for unselecting publishers
                for (int iii = 0; iii < UnselectedParts.length; iii++) {
                        if (UnselectedParts[iii].length()>1) { 
			Query unQuery = new TermQuery(new Term("publisher", UnselectedParts[iii]));
                        bqt2.add(unQuery, Occur.MUST_NOT);
			}
		}


                // use language constraint
                 if (Language.length() ==2 ) {
                        Query langQuery = new TermQuery(new Term("lang", Language));
                        bqt2.add(langQuery, Occur.MUST);
                 };


                // with constraint
                BooleanQuery query_standard=bqt2.build();
                TopDocs foundDocs1 = searcher.search(query_standard, Istart+Ihits);


		ScoreDoc[] hits1  = foundDocs1.scoreDocs;
		//System.out.println("StandardAnalyser Results :: " + foundDocs1.totalHits);

		RES="";

		int Nmax=Istart+Ihits;
		if (Nmax>hits1.length+hits_w.length) Nmax=hits1.length+hits_w.length;

		if (Istart<1) {
			if (hits1.length + hits_w.length<=0) RES=RES+"<h4 style=\"color:red;\">No results containing \""+SEARCH+"\" were found in the article titles.</h4>\n";
			else {
				if (type==0 || type>9) {
					RES=RES+"<h4>Search for \""+SEARCH+"\" in article titles:</h4>\n";
				}
			}}



		RES=RES+"<ol>\n";

	
		 ArrayList<String> usedTitle = new ArrayList<String>();
                 ArrayList<String> usedContent = new ArrayList<String>();


                // analyse titles...
	        boolean isCapital= SetEnv.checkCapitalOrDigit( SEARCH ); 
                //System.out.println(SEARCH);
		//System.out.println(isCapital);

		if (isCapital) { // if the word has a capital latter, it is some term, so run white analyser first  
		         RES = RES+searchInTitles( hits_w, searcher,type, usedTitle, true, Unselected, Language,isLocalZWI ); // white analyser 
                         // End of white analyser 
                         //System.out.println(RES+" --------- end white\n\n");
                         //System.exit(0);
			 RES = RES+searchInTitles( hits1, searcher,type, usedTitle, false, Unselected, Language,isLocalZWI ); // standard  
                } else {
                          RES = RES+searchInTitles( hits1, searcher,type, usedTitle, true , Unselected, Language,isLocalZWI); // standard  analyser 
                          RES = RES+searchInTitles( hits_w, searcher,type, usedTitle, false , Unselected, Language,isLocalZWI); // white standard  
                        }; 
	       RES=RES+"</ol>\n";


               int nn=0;
               float maxScore=1.0f;

		//  more like. if too few. Use fuzzy search of phrases, but only when title are looked at 
		if ((hits1.length+hits_w.length)<5 && type==0) {
			int MaxLike=20; // 20 is good 

			 //System.out.println("Search like..");
                        /*
			SpanQuery[] clauses = new SpanQuery[ SEARCH_WORDS.length ];

                         for (int ll=0; ll<SEARCH_WORDS.length; ll++) {
			    clauses[ll] = new SpanMultiTermQueryWrapper(new FuzzyQuery(new Term("title", SEARCH_WORDS[ll] ), 1));
                            //System.out.println(SEARCH_WORDS[ll]);
			 };

			 SpanNearQuery queryLike = new SpanNearQuery(clauses, 0, true); 
                         TopDocs likes = searcher.search(queryLike, MaxLike);
                         ScoreDoc[] hits1likes  = likes.scoreDocs;
                         */


			  /*
			  FuzzyQuery fuzzyNoSuch = new FuzzyQuery(new Term("title", SEARCH), 1, 0, 1, false);
                          SpanQuery spanNoSuch = new SpanMultiTermQueryWrapper<>(fuzzyNoSuch);
                          SpanQuery spanFirst = new SpanFirstQuery(spanNoSuch, 10);
                          TopDocs likes = searcher.search(spanFirst, MaxLike);
                         ScoreDoc[] hits1likes  = likes.scoreDocs;
                          */


			 // apply fuzziness for simple words


			ScoreDoc[] hits1likes = null;

			// single words
			if (SEARCH_WORDS.length == 1) {
                            FuzzyQuery fuzzyQuery = new FuzzyQuery(new Term("stitle", SEARCH)); // use short title for fuzzy 
                            //fuzzyQuery.setRewriteMethod(MultiTermQuery.SCORING_BOOLEAN_REWRITE);
                            TopDocs likes = searcher.search(fuzzyQuery, MaxLike);
                            hits1likes  = likes.scoreDocs;
                         }

       
                        // complex phrase 
                        if (SEARCH_WORDS.length > 1) {

			 String qString =SEARCH_WORDS[0]+"~";
                         for (int ll=1; ll<SEARCH_WORDS.length; ll++) {
                            qString = qString + " "+SEARCH_WORDS[ll]+"~"; 
                         };
   
			    //System.out.println( qString); 
	        	    ComplexPhraseQueryParser qp = new ComplexPhraseQueryParser("stitle", analyzer); // use short title 
                            qp.setInOrder(true);
                            qp.setFuzzyPrefixLength(1);
                            Query qphr = qp.parse(qString);
       			    TopDocs likes = searcher.search( qphr, MaxLike);
                            hits1likes  = likes.scoreDocs;
                         }



			 //System.out.println(hits1likes.length);

/*
			MoreLikeThis moreLikeThis=new MoreLikeThis(reader);
			moreLikeThis.setAnalyzer(analyzer);
			moreLikeThis.setFieldNames(new String[]{"title"});
			moreLikeThis.setMinTermFreq(1);
			moreLikeThis.setMinDocFreq(1);
			moreLikeThis.setMinWordLen(2);
			moreLikeThis.setBoost(true);
			ScoreDoc dsim = hits1[0];
			Query like =  moreLikeThis.like(dsim.doc);
			TopDocs likes = searcher.search(like, MaxLike);
			ScoreDoc[] hits1likes  = likes.scoreDocs;
*/



		        char firstChar = (SEARCH.toLowerCase()).charAt(0);

			Nmax=Istart+Ihits;
			if (Nmax>hits1likes.length) Nmax=hits1likes.length;


			// do not count first
			if (Nmax>0) {
                                Istart=0;
				String RES1="<h4>Suggestions for article titles:</h4>";
				RES1=RES1+"<ol>\n";
				int nsim=0;
				int NLL=0;
				maxScore=1.0f;
				for (int start = Istart; start < Nmax; start++) {
					int docId = hits1likes[start].doc;
					Document d = searcher.doc(docId);
					String sha1=d.get("sha1");
					if (usedTitle.contains( sha1 )) continue;
					usedTitle.add(  sha1  );

					String sID=d.get("hash"); // d.get("rowid"); // Just ID of an article 
					//int fID = Integer.parseInt(sID); /// proper page ID
					String full_path=d.get("full_path");
					String title=d.get("title"); // title without namespace
                                        //String lang=d.get("lang"); // title without namespace
					String stitle=d.get("stitle"); // title without namespace
					String topics=d.get("topics");
					String category=d.get("categories");
					String time=d.get("time_created");
					String publisher=d.get("publisher");
                                        String srating=d.get("rating");
					try {
                                            double rating = Double.parseDouble(srating);
                                            if (rating>5) continue; 
					} catch (NumberFormatException e) {};


					/*
					if (Language.length() ==2 ) {  
                                                boolean takeLanguageSpecific=false;
						if (Language.equals(lang)) takeLanguageSpecific=true;
                                                if (takeLanguageSpecific==false) continue; 
                			} else {}
                                        */


					// unselect publisher
                                        if (Unselected.indexOf(publisher)>-1) continue; 

					//if (publisher.indexOf("jewishenc")>-1) continue; 

					// assume first charcater match 
	                                if (SEARCH_WORDS.length==1) {	
					  char firstCharTitle = (title.toLowerCase()).charAt(0);
                                          if (firstCharTitle != firstChar) continue; 
                                        }

					String description=d.get("description");
                                        String ext_url=d.get("url");
                                        //String boost=d.get("boost");
                                        //System.out.println(boost);
                                        //System.out.println("OO");

					String categories=getFormattedCategories(category,publisher);
					//String sentance=SetEnv.getSentence(d.get("content"));
                                        String sentance=SetEnv.getSentence( description );

					if (title.length()<2) continue;
					nn++;
					if (NLL==0)  maxScore=(hits1likes[0].score);
					NLL=NLL+1;
					int score= (int)( (hits1likes[start].score / maxScore)*100);
					if (score<40) continue; // too low score for matching

					String iscore=String.valueOf(score);

					TYPE_WIKI=getWIKI(publisher);
				        String url=getEnURL(publisher,full_path,ext_url,sID,type,isLocalZWI);	
					String desc=getTopic(publisher, topics);
					RES1=RES1+"<li><span class=\"xitem\">";
                                        url = url.replaceAll("'","%27");
					//String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span> <span class=\"x2\"> "+ desc + " </span> "+ sentance+" <span style=\"color:#008000;font-size:80%;\"> "+time+"</span> "+categories;

                                        String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span>: "+sentance+" "+desc+" <span style=\"color:#008000;font-size:80%;\"> [" + iscore + "%] "+time+"</span> "+categories;

					RES1=RES1+tmp+"</span></li>\n\n";

					nsim++;
				}
				RES1=RES1+"</ol>\n";

				// if found, then show
				if (nsim>0) {
					RES=RES+RES1;
				}


			}



		} // end similar content



		// do not look for content when type is 0 (titles)
		if (type==0) return RES;

		/* --------------------------------- Content Whitespace -------------------------------*/

		PhraseQuery query2W = new PhraseQuery("content_w", SEARCH_WORDS);
		BooleanQuery.Builder bq = new BooleanQuery.Builder();
		bq.add(query2W, Occur.MUST);

                // condition for unselecting publishers
                for (int iii = 0; iii < UnselectedParts.length; iii++) {
                        if (UnselectedParts[iii].length()>1) { 
			Query unQuery = new TermQuery(new Term("publisher", UnselectedParts[iii]));
                        bq.add(unQuery, Occur.MUST_NOT);
			}
		}


                // use language constraint
                 if (Language.length() ==2 ) {
                        Query langQuery = new TermQuery(new Term("lang", Language));
                        bq.add(langQuery, Occur.MUST);
                 };



		BooleanQuery query_content_w=bq.build();

		//for (String word : mywords) {
		//        query.add(new Term("contents", word));
		// }



		// Start white space analysern
		//QueryParser parser2W = new QueryParser("content_w", analyzer_w);
		//parser2W.setDefaultOperator(QueryParser.Operator.AND);
		//parser2W.setPhraseSlop(0); // words togerger
		//Query query2W = parser2W.parse( SEARCH );

		TopDocs foundDocs2W = searcher.search(query_content_w,Istart+Ihits);
		ScoreDoc[] hits2W  = foundDocs2W.scoreDocs;
		//System.out.println("Whitespace Results in CONTENT :: " + hits2W.length);


		//Uses HTML &lt;B&gt;&lt;/B&gt; tag to highlight the searched terms
		//Formatter formatter = new SimpleHTMLFormatter( "<span style=\"background-color:#ffa500;\">", "</span>" );
                Formatter formatter = new SimpleHTMLFormatter( "<b>", "</b>" );

		//It scores text fragments by the number of unique query terms found
		//Basically the matching score in layman terms
		QueryScorer scorerW = new QueryScorer(query_content_w);

		//used to markup highlighted terms found in the best sections of a text
		Highlighter highlighterW = new Highlighter(formatter, scorerW);

		//It breaks text up into same-size texts but does not split up 60 char spans
		Fragmenter fragmenterW = new SimpleSpanFragmenter(scorerW, 70);

		//breaks text up into same-size fragments with no concerns over spotting sentence boundaries.
		//Fragmenter fragmenter = new SimpleFragmenter(10);
		//set fragmenter to highlighter
		highlighterW.setTextFragmenter(fragmenterW);

		String RES2="";
		//if (type<0 && hits2W.length>0) RES2=RES2+"<i>Nr of results: " + hits2W.length + "</i>\n";
		RES2=RES2+"<ol>\n";

		Nmax=Istart+Ihits;
		if (Nmax>hits2W.length) Nmax=hits2W.length;


		int NCon=0;

		maxScore=1.0f;
		for (int start = Istart; start < Nmax; start++) {
			int docId = hits2W[start].doc;
			Document d = searcher.doc(docId);
                        String sID=d.get("hash");  //   d.get("rowid"); // Just an ID of an article 
			String sha1=d.get("sha1");
			if (usedContent.contains( sha1 )) continue;
			usedContent.add(  sha1  );
			if (start == Istart)  maxScore=(hits2W[start].score);

			String title=d.get("title"); // full title
                        String stitle=d.get("stitle"); // short title 
                        //String lang=d.get("lang"); // title without namespace
			String topics=d.get("topics");
			String category=d.get("categories");
			String time=d.get("time_created");
			String full_path=d.get("full_path");
			String content=d.get("content");
			String publisher=d.get("publisher");
                        String srating=d.get("rating");
			try {
                             double rating = Double.parseDouble(srating);
                             if (rating>5) continue; 
                        } catch (NumberFormatException e) {};


			/*
                        if (Language.length() ==2 ) {
                                                boolean takeLanguageSpecific=false;
                                                if (Language.equals(lang)) takeLanguageSpecific=true;
                                                if (takeLanguageSpecific==false) continue;
                        } else {}
                        */


			// unselect publisher
                        //if (Unselected.indexOf(publisher)>-1) continue; 

		        //if (publisher.indexOf("jewishenc")>-1) continue; 

			String ext_url=d.get("url");

			if (title.length()<2) continue;
			int score= (int)( (hits2W[start].score / maxScore)*100);
			String iscore=String.valueOf(score);

			nn++;

			// get only 1 best section (not 10!)
			//String desc_part =  highlighter.getBestFragment(analyzer, "content", content);

			// get 3 best parts
			String[] frags =  highlighterW.getBestFragments(analyzer_w, "content_w", content, 3);
			String desc_part="...";
			for (String frag : frags)   {
				frag = frag.replace("&quot;","'");
				frag = frag.replace("&amp;","&");
				frag = frag.replace("&gt;",">");
				frag = frag.replace("&lt;","<");
				desc_part = desc_part+ frag+"...";
			};


			if (desc_part.length()<10) continue; // too short decription


			/*
			//Create token stream
			TokenStream stream = TokenSources.getAnyTokenStream(reader,  docId, "content", analyzer);
			String[] frags = highlighter.getBestFragments(stream, content, 2);

			String desc_part="...";             
			for (String frag : frags)   desc_part = desc_part+frag+"...";
			*/

			//String desc_part=getTextAround(content,SEARCH);


			String categories=getFormattedCategories(category, publisher);
			TYPE_WIKI=getWIKI(publisher);
		        String url=getEnURL(publisher,full_path,ext_url,sID,type,isLocalZWI);	
			String desc=getTopic(publisher, topics);
			RES2=RES2+"<li><span class=\"xitem\">\n";
                        url = url.replaceAll("'","%27");
			//String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a> </span> <span class=\"x2\"> "+ desc+" "+desc_part + " </span><span style=\"color:#008000;font-size:80%;\"> "+time+"</span> "+categories;

                        String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span>: " + desc_part+" "+desc + "<span style=\"color:#008000;font-size:80%;\"> [" + iscore + "%] "+time+"</span> "+categories;

			RES2=RES2+tmp+"</span></li>\n\n";
			NCon++;
		}

		/* --------------------------------- Content Standard -------------------------------*/


		//PhraseQuery query2 = new PhraseQuery("content", SEARCH_WORDS);
		//BooleanQuery.Builder bq3 = new BooleanQuery.Builder();
		//bq3.add(query2, Occur.MUST);


		QueryParser parser2 = new QueryParser("content", analyzer);
		parser2.setDefaultOperator(QueryParser.Operator.AND);
		parser2.setPhraseSlop(0); // words togerger
		Query query2 = parser2.parse( SEARCH );

                // add constraints
                BooleanQuery.Builder bqt4 = new BooleanQuery.Builder();
                bqt2.add(query2, Occur.MUST);

                // condition for unselecting publishers
                for (int iii = 0; iii < UnselectedParts.length; iii++) {
                        if (UnselectedParts[iii].length()>1) { 
			Query unQuery = new TermQuery(new Term("publisher", UnselectedParts[iii]));
                        bqt4.add(unQuery, Occur.MUST_NOT);
			}
		}


                // use language constraint
                 if (Language.length() ==2 ) {
                        Query langQuery = new TermQuery(new Term("lang", Language));
                        bqt4.add(langQuery, Occur.MUST);
                 };



                BooleanQuery query_content=bqt4.build();

		// in case of ? high load!
		//if  (SEARCH.indexOf("?") >-1 &&  SEARCH.length()>2) {
		//         query2 = new WildcardQuery(new Term("content", SEARCH));
		//         }



		TopDocs foundDocs2 = searcher.search(query_content,Istart+Ihits);
		ScoreDoc[] hits2  = foundDocs2.scoreDocs;
		//System.out.println("Standard Results in CONTENT :: " + hits2.length);


		//It scores text fragments by the number of unique query terms found
		//Basically the matching score in layman terms
		QueryScorer scorer = new QueryScorer(query_content);

		//used to markup highlighted terms found in the best sections of a text
		Highlighter highlighter = new Highlighter(formatter, scorer);

		//It breaks text up into same-size texts but does not split up 60 char spans
		Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 70);

		//breaks text up into same-size fragments with no concerns over spotting sentence boundaries.
		//Fragmenter fragmenter = new SimpleFragmenter(10);

		//set fragmenter to highlighter
		highlighter.setTextFragmenter(fragmenter);


		Nmax=Istart+Ihits;
		if (Nmax>hits2.length) Nmax=hits2.length;


		maxScore=1.0f;
		for (int start = Istart; start < Nmax; start++) {
			int docId = hits2[start].doc;
			Document d = searcher.doc(docId);
			String sha1=d.get("sha1");
			if (usedContent.contains( sha1 )) continue;
			usedContent.add(  sha1  );


			if (start == Istart)  maxScore=(hits2[start].score);

			String title=d.get("title"); // full title
		        String stitle=d.get("stitle"); // short title
                        String sID=d.get("hash"); // d.get("rowid"); // Just ID of a article 
			String topics=d.get("topics");
                        //String lang=d.get("lang"); // title without namespace
			String category=d.get("categories");
			String time=d.get("time_created");
			String full_path=d.get("full_path");
			String content=d.get("content");
			String publisher=d.get("publisher");
                        String srating=d.get("rating");
 			try {
                             double rating = Double.parseDouble(srating);
                             if (rating>5) continue; 
                        } catch (NumberFormatException e) {};


			/*
                        if (Language.length() ==2 ) {
                                                boolean takeLanguageSpecific=false;
                                                if (Language.equals(lang)) takeLanguageSpecific=true;
                                                if (takeLanguageSpecific==false) continue;
                        } else {}
                        */

			// unselect publisher
                        //if (Unselected.indexOf(publisher)>-1) continue; 

			//if (publisher.indexOf("jewishenc")>-1) continue; 

			String ext_url=d.get("url");

			if (title.length()<2) continue;
			int score= (int)( (hits2[start].score / maxScore)*100);
			String iscore=String.valueOf(score);

			nn++;

			// get only 1 best section (not 10!)
			//String desc_part =  highlighter.getBestFragment(analyzer, "content", content);

			// get 3 best parts
			String[] frags =  highlighter.getBestFragments(analyzer, "content", content, 3);
			String desc_part="...";
			for (String frag : frags)   {
				frag = frag.replace("&quot;","'");
				frag = frag.replace("&amp;","&");
				frag = frag.replace("&gt;",">");
				frag = frag.replace("&lt;","<");
				desc_part = desc_part+ frag+"...";
			};

			if (desc_part.length()<10) continue; // too short decription


			/*
			//Create token stream
			                   TokenStream stream = TokenSources.getAnyTokenStream(reader,  docId, "content", analyzer);
			String[] frags = highlighter.getBestFragments(stream, content, 2);

			                   String desc_part="...";	     
			for (String frag : frags)   desc_part = desc_part+frag+"...";
			                   */ 

			//String desc_part=getTextAround(content,SEARCH);


			String categories=getFormattedCategories(category, publisher);
			TYPE_WIKI=getWIKI(publisher);
	                String url=getEnURL(publisher,full_path,ext_url,sID,type,isLocalZWI);	
			String desc=getTopic(publisher, topics);
			RES2=RES2+"<li><span class=\"xitem\">\n";
                        url = url.replaceAll("'","%27");
			String tmp="<span class=\"x1\"> "+ TYPE_WIKI + " <a href=\"" + url +"\">"+title+"</a></span>: "+ desc_part+" "+desc + " <span style=\"color:#008000;font-size:80%;\">[" + iscore + "%] "+time+"</span> "+categories;
			RES2=RES2+tmp+"</span></li>\n\n";
			NCon++;
		}

		RES2=RES2+"</ol>\n";


		if (NCon>0)  RES=RES+"<h4>Search for \"" + SEARCH + "\"  in the article content:</h4>\n"+RES2;
		if (NCon==0)  RES=RES+"<h4>No results for \"" + SEARCH +"\" in the article content</h4>\n";

		// next result
		/*
		if (hits2.length+hits1.length<20){
		        String next="There were no many results... <H3><a href='/find.php?query="+SEARCH+"'>Use search of external encyclopedias?</a></H3> <p></p> ";
		        RES=RES+next+"\n";
	}
		*/


		/*
				// next result
				if (Ihits <= hits2.length){
					String ss=new String(word1);
					String ss2=new String(word2);
					String s=String.valueOf(Nmax);
					String next="<H3><a href='/wiki/search/form.php?query="+ss+"&start="+s+"'>Next results</a></H3> <p></p> ";
					if (ss2.length()>1) next="<H3><a href='wiki/search/form.php?query="+ss+"+"+ss2+"&start="+s+"'>Next results</a></H3> <p></p> ";
					RES=RES+next+"\n";
				}
		*/

		return RES;

	}


	// get nice categories (up to 3)
	private static String getFormattedCategories(String category, String publish){
		if (category==null) return "";
		if (category.length()<3) return "";
		String app="";
		int n=0;
		int MaxCat=1;
		String categories="";

		String[] arrSplit = category.split(";");
		for (String a : arrSplit) {
			//categories=categories+" [<a href=\"/wiki/Category:"+a+"\">"+a+"</a>]";
			categories=categories+" [<a href=\"/r/"+publish+".php?q=Category:"+a+"\">"+a+"</a>]";
			n++;
			if (n>MaxCat){
				app = "...";
				break;
			};
		};

		categories=categories+app;
		return "<span style=\"font-size:80%;\">"+categories+"</span>";


	};


}
